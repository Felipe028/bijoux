<?php
require("conexao.php");
require("verifica_login.php");
require("funcoes/despesas/vereficaDespesaFixa.php");
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="img/logo_1.png">
<title>100% Bijoux</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
<script type="text/javascript" src="js/tabela-add-compra.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.1.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script>
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );

        new dgCidadesEstados1( 
            document.getElementById('estado1'), 
            document.getElementById('cidade1'), 
            true
        );		
    }
</script>
<script>
 function calcular() {
    var num1 = Number(document.getElementById("num1").value);
    var num2 = Number(document.getElementById("num2").value);
    var elemResult = document.getElementById("resultado");

    if (elemResult.textContent === undefined) {
       elemResult.textContent = "R$" + String(num1 - num2);
    }
    else { // IE
       elemResult.innerText = "R$" + String(num1 - num2);
    }
}
</script>
</head>

<body>

<div class="sair" style="width: 920px; margin:0 auto; text-align: right;">
<?php echo $email;?> | <a href="logout.php" class="ver-todos">Sair</a>
</div>

<div class="geral">
    <div class="topo" id="topo">
	
        <figure style="position: relative; margin: 0px 0px 10px 0px; ">
            <center><img src="img/topo_3.png"/></center>
	        <figcaption><!--legenda da imagem-->
			    <p style="font-size:600%; font-family: forte; position: absolute; top: 0px; width: 600px; height: 110px; margin: 8px 0px 0px 250px;">100% Bijoux</p>
				<p style="font-size:125%; font-family: Monotype Corsiva; position: absolute; top: 0px; width: 200px; height: 40px; margin: 109px 0px 0px 594px;">Acessórios & Bijuterias</p>
	        </figcaption>
       </figure>

<input type="hidden" value="<?php $data = date("Y-m-d", mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"), 0));?>">
<?php
$data3 = date('Y-m-d', strtotime("+3 days",strtotime($data)));
?>
<?php
$sql = mysql_query("SELECT * FROM parcela WHERE data_2 BETWEEN '$data' AND '$data3' AND status_2 = '0'");
$contd = 0; ?>
<?php while($ln = mysql_fetch_array($sql)){$contd = $contd + 1;} ?>

        <ul>
			<li><a style="margin:0px 0px 0px -38px;" href="Index.php?p=vend">Vendas</a></li>
			<li><a href="Index.php?p=prod">Produtos</a></li>
			<li><a href="Index.php?p=forn">Fornecedores</a></li>
			<li><a href="Index.php?p=clie">Clientes</a></li>
			<li><a href="Index.php?p=func">Funcionários</a></li>
			<li><a href="Index.php?p=desp">Despesas</a>
			<?php if($contd == 0 || $_GET['p']  == 'desp'){}else{ ?> <p style="width: 15px; height: 15px; background: #f70707; -moz-border-radius:10px; -webkit-border-radius:10px;  text-align: center; color: #ffffff; font-size:80%; position: absolute; margin: 2px 0px 0px 745px;"><?php echo $contd;?></p> <?php } ?>
			</li>
			<li><a href="Index.php?p=rela">Relatórios</a></li>
		</ul>
    </div>
	
    <div class="conteudo"> 
        <?php
        include "conexao.php";
        include "pagina.php";
		
        ?>	  
    </div>
</div>

</body>
</html>