<?php
include "../conexao.php";
include "../verifica_login.php";
require_once('../pdf/mpdf.php');
$cod = substr(base64_decode($_GET['cod']), 6, 4);

$pagina = '
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="../img/logo_1.png">
<title>UFAM</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="../js/jquery.reveal.js"></script>
<script type="text/javascript" src="../js/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" href="../css/reveal.css">
<script type="text/javascript" src="../js/scripts.js"></script>

</head>

<body class="body">
';
$sql = mysql_query("SELECT * FROM curso_has_disciplina WHERE curso_cod = '$cod' AND versao_idversao = '".$_GET['versao']."' ORDER BY periodo ASC");
$cont = 1;
while ($ln = mysql_fetch_array($sql)){
	if($cont == $ln['periodo']){
    $pagina .=' <p style="width: 100%; height: 30px; background: #b7b7b7; text-align: center; font-size:135%; margin: 10px 0px 4px 0px;">'.$ln['periodo'].'º PERIODO</p>';
    $cont ++;
	}else{
	$pagina .=' <p style="width: 100%; height: 1px; background: #000000; margin: 10px 0px 4px 0px;"></p>';
	}

$pagina .='<div style="width: 100%; height: 70px;">

<div style="float: left; width: 50%;">';
$sqldisc = mysql_query("SELECT * FROM disciplina WHERE cod = '".$ln['disciplina_cod']."'");
$lndisc = mysql_fetch_array($sqldisc);
$pagina .='<p style="margin: 0px;"><strong>DISCIPLINA: </strong>'.$lndisc['nome'].'</p>
<p style="margin: 0px;"><strong>CARGA HORÁRIA: </strong>'.$lndisc['ch_total'].' horas</p>
<p style="margin: 0px;"><strong>PRE-REQUISITOS: </strong>';
$sqlpre = mysql_query("SELECT * FROM prerequisito WHERE curso_has_disciplina_curso_cod = '$cod' AND curso_has_disciplina_disciplina_cod = '".$ln['disciplina_cod']."'");
while ($lnpre = mysql_fetch_array($sqlpre)){
$pagina .='	'.$lnpre['pre_requisito'].' '; 
}
$pagina .='</p>
</div>

<div style="float: right; width: 40%;">
<p style="margin: 0px;"><strong>SIGLA: </strong>'.$ln['disciplina_cod'].'</p>
<p style="margin: 0px;"><strong>CRÉDITOS: </strong>'.$lndisc['cred_total'].'</p>
<p style="margin: 0px;"></p>
</div>

</div>

<div style="width: 100%;">
<p style="margin: 0px;"><strong>EMENTA:</strong></p>
<p style="margin: 0px;">';
$sqlEa = mysql_query("SELECT * FROM disciplina_has_ementa WHERE disciplina_cod = '".$ln['disciplina_cod']."'");
	while($lnEa = mysql_fetch_array($sqlEa)){
		$sqlEmenta = mysql_query("SELECT * FROM ementa WHERE cod = '".$lnEa['ementa_cod']."'");
		$lnEmenta = mysql_fetch_array($sqlEmenta);
$pagina .= '
	    '.$lnEmenta['descricao'].'. 
';
	}
$pagina .= '
</p>
</div>

<div style="width: 100%; margin: 20px 0px 0px 0px;">
<p style="margin: 0px;"><strong>OBJETIVO GERAL:</strong></p>
<p style="margin: 0px;">'.$lndisc['objetivo'].'</p>
</div>

<div style="width: 100%; margin: 20px 0px 0px 0px;">
<p style="margin: 0px;"><strong>OBJETIVO ESPECÍFICO:</strong></p>
<p style="margin: 0px;">'.$lndisc['objetivo_especifico'].'</p>
</div>

';
}
$pagina .='</body>
</html>
';
$mpdf = new mPDF('c', 'A4');
$mpdf->WriteHTML($pagina);
$css = file_get_contents('../css/style.css');
$mpdf->WriteHTML($css, 1);
$mpdf->Output('report.pdf', 'I');

// I - Abre no navegador
// F - Salva o arquivo no servido
// D - Salva o arquivo no computador do usuário
?>
