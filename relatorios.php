<div style="width: 60%; height: 200px; text-align: center; margin: 110px auto 0px auto;">
 <a href="#" data-reveal-id="diario" data-animation="fade" style="margin: 0px auto 0px auto;">
<div class="divhrefrel" style="text-align: center; -webkit-border-radius:5px; -moz-border-radius:5px;">
Imprimir relatório diário
</div>
</a>
<br/>

<a href="#" data-reveal-id="mensal" data-animation="fade" style="margin: 0px 0px 0px 0px;  -moz-border-radius:5px 0px 0px 5px; -webkit-border-radius:5px 0px 0px 5px;">
<div class="divhrefrel" style=" text-align: center; ">
Imprimir relatório mensal
</div>
</a>
</div>
		<!--DIARIO-->
		<div id="diario" class="reveal-modal">
			<p style="margin: 0px auto 0px auto; padding: 0px 0px 10px 0px; text-align: center; font-family: Lucida Calligraphy; font-size:155%; color: #949494;">Relatório Diário</p>
			
			<form method="post" action="relatorio/diario.php" target="_blank">
			<div style="background: #f4f4f4; -webkit-border-radius:6px; -moz-border-radius:6px; border-bottom:2px solid #dbdbdb; height: 38px; padding: 10px 0px 0px 0px;">
			
			<p style="color: #717171; margin: 0px 0px 0px 84px; height: 30px;">Selecionar dia:
			<input style="height: 24px; border:1px solid #aaaaaa; margin: -4px 0px 10px 0px;" type="date" name="dia">
			<input class="bt-input" type="submit" value="Imprimir">
			</p>
			
			</div>			
			<a href="" class="close-reveal-modal"> x</a>
			</form>
		</div>
        <!--fim-->		

		<!--MENSAL-->
		<div id="mensal" class="reveal-modal">
			<p style="margin: 0px auto 0px auto; padding: 0px 0px 10px 0px; text-align: center; font-family: Lucida Calligraphy; font-size:155%; color: #949494;">Relatório Mensal</p>
			
			<form method="post" action="relatorio/mensal.php" target="_blank">
			<div style="background: #f4f4f4; -webkit-border-radius:6px; -moz-border-radius:6px; border-bottom:2px solid #dbdbdb; height: 38px; padding: 10px 0px 0px 0px;">
			<p style="color: #717171; margin: 0px 0px 0px 90px; height: 30px;">Selecionar mês:
			<select name="mes" style="width: ; height: 26px; margin: 0px 0px 10px 0px;">
			<option value="01">Janeiro</option>
			<option value="02">Fevereiro</option>
			<option value="03">Março</option>
			<option value="04">Abril</option>
			<option value="05">Maio</option>
			<option value="06">Junho</option>
			<option value="07">Julho</option>
			<option value="08">Agosto</option>
			<option value="09">Setembro</option>
			<option value="10">Outubro</option>
			<option value="11">Novembro</option>
			<option value="12">Dezembro</option>
			</select>
			
			<input type="hidden" value="<?php $data = date("Y-m-d", mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"), 0));?>">
			<?php $rest = substr($data, 0, -6); $ano = "2016";?>
			
			
			<select name="ano" style="width: ; height: 26px; margin: 0px 0px 10px 0px;">
			<?php  while($ano <= $rest){ ?>
			<option value="<?php echo $ano;?>"><?php echo $ano; $ano ++;?></option>
			<?php } ?>
			</select>
			
			<input class="bt-input" type="submit" value="Imprimir">
			</p>
			</div>
			<a href="" class="close-reveal-modal"> x</a>
			</form>
		</div>
        <!--fim-->		