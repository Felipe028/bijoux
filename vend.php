<?php
require("conexao.php");
require("verifica_login.php");
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="img/logo_1.png">
<title>100% Bijoux</title>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!--Reveal modal-->
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<link rel="stylesheet" href="css/reveal.css">
<!--Fim Reveal modal-->
<script type="text/javascript" src="js/tabela-add-compra.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.1.js"></script>
<script type="text/javascript" src="js/cidades-estados-v0.2.js"></script>
<script type="text/javascript">
    window.onload = function() {
        new dgCidadesEstados( 
            document.getElementById('estado'), 
            document.getElementById('cidade'), 
            true
        );

        new dgCidadesEstados1( 
            document.getElementById('estado1'), 
            document.getElementById('cidade1'), 
            true
        );		
    }
</script>
<script>
 function calcular() {
    var num1 = Number(document.getElementById("num1").value);
    var num2 = Number(document.getElementById("num2").value);
    var elemResult = document.getElementById("resultado");

    if (elemResult.textContent === undefined) {
       elemResult.textContent = "R$ " + String(num1 - num2);
    }
    else { // IE
       elemResult.innerText = "R$ " + String(num1 - num2);
    }
}
</script>
</head>

<body>
<div style="width: 100%; background: #8ace1e; height: 60px;">
<p style="font-size:250%; font-family: forte; position: absolute; top: 0px; width: 600px; height: 110px; margin: 2px 0px 0px 40px;">100% Bijoux</p>
<p style="font-size:74%; font-family: Monotype Corsiva; position: absolute; top: 0px; width: 200px; height: 40px; margin: 46px 0px 0px 154px;">Acessórios & Bijuterias</p>
</div>

<div style="float: left; width: 80%; ">
<?php
$selecionaDados = mysql_query("SELECT * FROM venda WHERE idVenda = '".$_GET['id']."'");
$campos = mysql_fetch_array($selecionaDados);

if($campos['status_2'] == 1 || $campos['funcionario_idFuncionario'] != $_SESSION['idFuncionario']){
	echo "<script>location = 'funcoes/vendas/cadastro_venda.php'; </script>";
}
?>

				<table id="tabela" style="margin: 0px auto 0px auto;">
				    <tbody>
					    <tr class="tabela-new-venda">
						    <th style="width: 15%;">Cod</th>
							<th style="width: 45%;">Produto</th>
							<th style="width: 5%;">Qtde</th>
							<th style="width: 20%;">Preço</th>
							<th style="width: 15%;">Ações</th>
						</tr>
						<form method="post" action="funcoes/vendas/cadastro_venda2.php?id=<?php echo $campos['idVenda'];?>">
						<tr style="height: 36px;" class="tabela-new-venda2">
						    <td style="border-left:1px solid #dbdbdb; text-align: center;">
							<!--input para receber somente "numeros"-->
							<input type="text" name="codigo" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="text-align: center; width:100px; height: 20px; margin: 0px 0px 0px 0px;"/>
							<!--FIM input-->
							</td>
							<td></td>
							<td style="text-align: center;"><input type="number" name="quantidade" style="width: 42px; height: 20px; text-align: center; margin: 0px 0px 0px 5px;" value="1"/></td>
							<td></td>
							<td style="border-right:1px solid #dbdbdb; text-align: center; padding: 1px 0px 0px 0px;"><input style="width:90px;" class="bt-input" type="submit" value="Add"/></td>
						</tr>
						</form>
						<?php
	                    $sql = mysql_query("SELECT * FROM produto_has_venda WHERE venda_idVenda = '".$campos['idVenda']."'");
	                    $contqtde = 0;
						$contpreco = 0;
	                    while($ln = mysql_fetch_array($sql)){
							$sqlprod = mysql_query("SELECT * FROM produto WHERE idProduto = '".$ln['produto_idProduto']."'");
							$lnprod = mysql_fetch_array($sqlprod); ?>
							<input type="hidden" value="<?php $contqtde = $contqtde + $ln['quantidade'];?>">
							<input type="hidden" value="<?php $contpreco = $contpreco + ($lnprod['preco'] * $ln['quantidade']);?>">			
							
                        <tr style="height: 36px;" class="tabela-new-venda2">
						    <td style="border-left:1px solid #dbdbdb; text-align: center;"><?php echo $ln['produto_idProduto'];?></td>
							<td style=""><?php echo $lnprod['nome_prod'];?></td>
							<td style="text-align: center;"><?php echo $ln['quantidade'];?></td>
							<td style="text-align: center;">R$ <?php echo $lnprod['preco'];?></td>
							
							<td style="border-right:1px solid #dbdbdb; text-align: center; padding: 6px 0px 0px 0px;">
							<a href="#" title="Imagem" data-reveal-id="vis<?php echo $ln['produto_idProduto'];?>" data-animation="fade" class="bt-visualizar"><img src="img/bt-foto.png" style="width: 20px; height: 20px; margin: 0px 0px 0px 0px;"/></a>
							<a href="#" title="Excluir" data-reveal-id="del<?php echo $ln['produto_idProduto'];?>" data-animation="fade" class="bt-remover"><img src="img/bt-excluir.png" style="width: 20px; height: 20px; margin: 0px 0px 0px 0px;"/></a>
							</td>
						</tr>
						<!--VISUALIZAÇÃO DA IMAGEM-->
						<div id="vis<?php echo $ln['produto_idProduto'];?>" class="reveal-modal" style="text-align: center;">
							<img src="imagens/<?php echo $lnprod['imagem'];?>" style="width: 100%; margin: -30px 0px -38px 0px;"/>
							<a href="" class="close-reveal-modal"> x</a>
						</div>
						<!--fim visualização-->						
	                    <!--exlusao-->
						    <div id="del<?php echo $ln['produto_idProduto'];?>" class="reveal-modal">
							    <h2 style="margin:0;padding:0; color: #eb0606;">Deseja relmente remover este item da lista?</h2>
								<form method="post" action="funcoes/vendas/excluir_venda2.php?cod=<?php echo $ln['produto_idProduto'];?>&id=<?php echo $campos['idVenda'];?>">
								    <input style="margin: 10px 0px 0px 410px;" class="bt-remover2" type="submit" value="SIM"  />
				                    <a href="" class="close-reveal-modal"> x</a>
								</form>
							</div>
						<!--fim exlusao-->
						<?php } ?>
					
										
					</tbody>
				
				    <tfoot>
					    <tr style="height: 30px;">
						    <td style="text-align: center;  background: #c6c5c5; height: 30px; text-shadow: 0 1px 0 #000000;">Total</td>
							<td style="background: #c6c5c5; text-shadow: 0 1px 0 #4F0000;"></td>
							<td style="text-align: center; background: #c6c5c5; text-shadow: 0 1px 0 #000000;"><?php if($contqtde==0){echo '0';}else{echo $contqtde;}?></td>
							<td style="text-align: center; background: #c6c5c5; text-shadow: 0 1px 0 #000000;"><?php if($contpreco==0){echo '0.00';}else{echo "R$ ".$contpreco."";}?></td>
							<td style="text-align: center; background: #c6c5c5;"></td>
						</tr>
					</tfoot>
			    </table>
</div>




<div style="float: right; margin: 0px 0px 0px 1px; width: 19%; height: 220px; background: #dbdbdb; padding: 0px 0px 0px 10px;">
<form method="post" action="funcoes/vendas/concluir.php?id=<?php echo $campos['idVenda'];?>">
<input type="text" id="num1" onblur="calcular();" style="display: none;" value="<?php echo $contpreco;?>"/>
<p style="margin: 8px 0px 0px 4px; font-size:120%;"><strong>Desconto<strong></p>
<p style="margin: 0px;"><!--input para receber somente "numeros" e "."-->
<input type="text" id="num2" name="num2" onblur="calcular();" placeholder="0.00" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == 46 || event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 118px; height: 22px; text-align: left; font-size:120%;" value="<?php echo $ln['preco'];?>"/><!--FIM input--></p>
							
<p style="margin: 6px 0px 0px 4px; font-size:120%;"><strong>Total a pagar<strong></p>
<p style="margin: 0px; border: 1px solid #b1b1b1; font-size:120%; width: 120px; height: 23px; background: #ffffff; padding: 3px 0px 0px 0px;"><strong> <span id="resultado"></span><strong></p>

<p style="margin: 6px 0px 0px 4px; float: left; font-size:120%;">Forma de pagamento</p>
<div style="display:none; float: right; margin: 0px 12px 0px 0px;" id="inputOculto1">
	<!--input para receber somente "numeros"-->
	<input type="text" name="cpf" placeholder="CPF" onKeypress="if (event.keyCode >= 32) event.returnValue = false; <?php $cont = 48; for($cont=48; $cont<=57;$cont++){?>if (event.keyCode == <?php echo $cont;?>) event.returnValue = true;<?php } ?>" style="width: 130px; height: 20px; font-size:120%;" value="<?php echo $ln['cpf'];?>" maxlength="11"/>
	<!--FIM input-->
	</div>
<p><select id="mySelect" name="pagamento" style="height: 26px; margin: 0px 16px 0px 0px;">
<option value="1">Dinheiro</option>
<option value="2">Cartão</option>
<option value="4">Crediário</option>
<!--<option value="3">Dinheiro/Cartão</option>-->
</select>
</p>

<input type="hidden" name="preco" value="<?php echo $contpreco;?>">
<a href="#" data-reveal-id="cancel" data-animation="fade" class="bt-cancelar">Cancelar</a>
<input style="width:100px; font-size:110%;" class="bt-input2" type="submit" value="Concluir"/>
</div>						
</form>
	


<!--Cacelar-->
<div id="cancel" class="reveal-modal">
    <h2 style="margin:0;padding:0; color: #eb0606; margin: 0px 0px 0px 40px;">Deseja realmente cacelar esta venda?</h2>
    <form method="post" action="funcoes/vendas/cancelar.php?id=<?php echo $_GET['id'];?>">
        <input style="margin: 10px 0px 0px 382px;" class="bt-remover2" type="submit" value="SIM"  />
        <a href="" class="close-reveal-modal"> x</a>
   </form>
</div>
<!--fim exlusao-->

<div style="position: absolute; top: 0px; right: 20px;">
<a href="Index.php?p=vend" style="color: #3e3939; margin: 0px 0px 0px 0px; font-size:110%; ">
<div class="divhref2" style="float: left; width: 70px; height: 60px; text-align: center;"><p style="margin: 18px 0px 0px 0px;">Menu</p></div>
</a>
<a href="logout.php" style="color: #3e3939; margin: 0px 0px 0px 0px; font-size:110%;">
<div class="divhref2" style="float: left; width: 70px; height: 60px; text-align: center;"><p style="margin: 18px 0px 0px 0px;">Sair</p></div>
</a>
</div>
</body>
</html>