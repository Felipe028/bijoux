--
-- Banco de Dados: `bijoux`
--
CREATE DATABASE `bijoux` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bijoux`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `idcliente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `endereco` varchar(60) DEFAULT NULL,
  `bairro` varchar(60) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `cpf` varchar(20) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `data_nasc` date DEFAULT NULL,
  `excluir` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nome`, `endereco`, `bairro`, `cep`, `cpf`, `telefone`, `email`, `data_nasc`, `excluir`) VALUES
(1, 'Felipe da Silva Arraes', 'Rua Antônio Reto; N 122', 'Iracy', '69101179', '00480208263', '993750971', 'felipearraes28@gmail.com', '2015-11-30', 0),
(2, 'Luan Rafael Arraes Suwa', 'Rua Antônio Reto; N 1221', 'Iracy', '69101179', '00480208261', '9937509711', '', '2015-11-29', 0),
(3, 'Fernada Castro Arruda Tavares', 'Rua Antônio Reto; N 122', 'Iracy', '69101179', '97886212731', '92992252982', 'nanda@gmail.com', '1990-11-13', 0),
(4, 'Lucas GAYbriel Arraes Suwa', '', '', '', '00480208299', '9343345353', '', '0000-00-00', 0),
(5, 'Fernada Castro Arruda Tavares', '', '', '', '', '993750971', '', '2016-11-15', 0),
(6, 'Anasj', '', '', '', '12567262545', '', '', '0000-00-00', 0),
(7, 'Ahsdhskj', '', '', '', '86573343894', '992252982', '', '2016-11-15', 0),
(8, 'Akhdksjjhsd', '', '', '', '82674979874', '', '', '0000-00-00', 0),
(9, 'Bafls', '', '', '', '00937484727', '', '', '0000-00-00', 0),
(10, 'Dkjdjfj', '', '', '', '27347432000', '', '', '0000-00-00', 0),
(11, 'Djfhhf', '', '', '', '88497429928', '', '', '0000-00-00', 0),
(12, 'Mjdkjfhsk', '', '', '', '72382039842', '', '', '0000-00-00', 0),
(13, 'Mkjdhf', '', '', '', '29829378392', '', '', '0000-00-00', 0),
(14, 'Mjsjdj', '', '', '', '09390940002', '', '', '0000-00-00', 0),
(15, 'Mkasaasn', '', '', '', '19212381723', '', '', '0000-00-00', 0),
(16, 'Kkaksjhksfs', '', '', '', '00990340923', '', '', '0000-00-00', 0),
(17, 'Kjhsjkd', '', '', '', '0092092092', '', '', '0000-00-00', 0),
(18, 'Klddsw', '', '', '', '00122291212', '', '', '0000-00-00', 0),
(19, 'Lpowls', '', '', '', '22209212901', '', '', '0000-00-00', 0),
(20, 'Plksl', '', '', '', '00192129102', '', '', '0000-00-00', 0),
(21, 'Maaaa', '', '', '', '19200919002', '', '', '0000-00-00', 0),
(22, 'Plosl', '', '', '', '01291212121', '', '', '0000-00-00', 0),
(23, 'Lamd', '', '', '', '28838283455', '', '', '0000-00-00', 0),
(24, 'Amdjs', '', '', '', '92829320932', '', '', '0000-00-00', 0),
(25, 'Amdksjs', '', '', '', '02923098420', '', '', '0000-00-00', 0),
(26, 'Asnsnds', '', '', '', '01298319029', '', '', '0000-00-00', 0),
(27, 'Maskdjal', '', '', '', '01928310273', '', '', '0000-00-00', 0),
(28, 'Amnjfshd', '', '', '', '83482638742', '', '', '0000-00-00', 0),
(29, 'Hasdhaskj', '', '', '', '09001902831', '', '', '0000-00-00', 0),
(30, 'Jkasjalksda', '', '', '', '10729874298', '', '', '0000-00-00', 0),
(31, 'MAjhakjhda', '', '', '', '72726387423', '', '', '0000-00-00', 0),
(32, 'Aqweqweq', '', '', '', '12347232730', '', '', '0000-00-00', 0),
(33, 'Ytaysaassus', '', '', '', '09203920324', '', '', '0000-00-00', 0),
(34, 'Oaaskdjsd', '', '', '', '09019765243', '', '', '0000-00-00', 0),
(35, 'Mjshjaa', '', '', '', '97238423746', '', '', '0000-00-00', 0),
(36, 'Lpasoa', '', '', '', '42638462834', '', '', '0000-00-00', 0),
(37, 'KAjka', '', '', '', '08283749236', '', '', '0000-00-00', 0),
(38, 'Popqoiqa', '', '', '', '12887289292', '', '', '0000-00-00', 0),
(39, 'Taajsa', '', '', '', '88718276182', '', '', '0000-00-00', 0),
(40, 'Maaia', '', '', '', '09823904230', '', '', '0000-00-00', 0),
(41, 'Mkisjaaa', '', '', '', '09975847584', '', '', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `crediario`
--

CREATE TABLE IF NOT EXISTS `crediario` (
  `idcrediario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `venda_idVenda` int(10) unsigned NOT NULL,
  `dinheiro` double NOT NULL,
  `cartao` double NOT NULL,
  `data_2` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  PRIMARY KEY (`idcrediario`),
  KEY `crediario_FKIndex1` (`venda_idVenda`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Extraindo dados da tabela `crediario`
--

INSERT INTO `crediario` (`idcrediario`, `venda_idVenda`, `dinheiro`, `cartao`, `data_2`, `hora`) VALUES
(12, 18, 0, 1, '2016-09-28', '02:22:55'),
(11, 18, 1, 0, '2016-09-28', '02:22:42'),
(13, 18, 1.5, 0, '2016-09-28', '02:23:12'),
(14, 18, 1, 1, '2016-09-28', '02:24:11'),
(15, 18, 2, 2.5, '2016-09-28', '03:00:42'),
(16, 19, 2, 1, '2016-09-30', '03:17:51'),
(24, 50, 5, 2, '2016-10-22', '15:48:29'),
(23, 43, 8, 0, '2016-10-11', '13:33:03'),
(22, 43, 2, 0, '2016-10-11', '13:32:47'),
(21, 43, 2, 0, '2016-10-11', '10:28:03'),
(25, 50, 2, 0, '2016-10-31', '17:39:14'),
(26, 90, 10, 0, '2016-11-05', '16:12:37'),
(27, 90, 0, 10, '2016-11-05', '16:12:48'),
(28, 90, 2, 1, '2016-11-05', '16:54:52'),
(29, 156, 0, 0, '2017-01-14', '18:17:19'),
(30, 156, 20, 0, '2017-01-14', '18:24:57'),
(31, 156, 0, 10, '2017-01-14', '18:40:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `despesas`
--

CREATE TABLE IF NOT EXISTS `despesas` (
  `iddespesas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao_despesa` varchar(200) DEFAULT NULL,
  `data_despesa` date DEFAULT NULL,
  `hora_despesa` time DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `tipo` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`iddespesas`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `despesas`
--

INSERT INTO `despesas` (`iddespesas`, `descricao_despesa`, `data_despesa`, `hora_despesa`, `valor`, `tipo`) VALUES
(17, 'Ar9', '2017-01-08', '02:57:33', 87, 2),
(18, 'Ar11', '2017-01-08', '02:58:15', 768, 2),
(16, 'Ar', '2017-01-08', '02:19:57', 600, 2),
(19, 'Ar10', '2017-01-08', '03:13:43', 121, 2),
(20, 'Aluguel', '2017-01-13', '15:56:05', 600, 1),
(21, 'Ar', '2017-01-13', '15:57:27', 100, 2),
(22, 'merenda', '2017-01-13', '15:58:26', 12, 0),
(23, 'Merenda', '2017-01-14', '17:42:36', 5, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE IF NOT EXISTS `fornecedor` (
  `idFornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_fornecedor` varchar(30) DEFAULT NULL,
  `endereco_fornecedor` varchar(30) DEFAULT NULL,
  `bairro_fornecedor` varchar(30) DEFAULT NULL,
  `cep_fornecedor` varchar(30) DEFAULT NULL,
  `cidade_fornecedor` varchar(30) DEFAULT NULL,
  `estado_fornecedor` varchar(30) DEFAULT NULL,
  `cnpj` varchar(30) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idFornecedor`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`idFornecedor`, `nome_fornecedor`, `endereco_fornecedor`, `bairro_fornecedor`, `cep_fornecedor`, `cidade_fornecedor`, `estado_fornecedor`, `cnpj`, `telefone`, `email`) VALUES
(1, 'Romanel', 'Rua Antônio Reto; N 122', 'Iracy', '69101179', 'Barcelos', 'AM', '12313111232312', '9329399342', '@gmail.com'),
(2, 'A', '', '', '', '', '', '', '', ''),
(3, 'B', '', '', '', '', '', '', '', ''),
(4, 'D', '', '', '', '', '', '', '', ''),
(5, 'C', '', '', '', '', '', '', '', ''),
(6, 'F', '', '', '', '', '', '', '', ''),
(7, 'E', '', '', '', '', '', '', '', ''),
(8, 'G', '', '', '', '', '', '', '', ''),
(9, 'H', '', '', '', '', '', '', '', ''),
(10, 'I', '', '', '', '', '', '', '', ''),
(11, 'J', '', '', '', '', '', '', '', ''),
(12, 'L', '', '', '', '', '', '', '', ''),
(13, 'M', '', '', '', '', '', '', '', ''),
(14, 'N', '', '', '', '', '', '', '', ''),
(15, 'O', '', '', '', '', '', '', '', ''),
(16, 'P', '', '', '', '', '', '', '', ''),
(17, 'Q', '', '', '', '', '', '', '', ''),
(18, 'R', '', '', '', '', '', '', '', ''),
(19, 'S', '', '', '', '', '', '', '', ''),
(20, 'T', '', '', '', '', '', '', '', ''),
(21, 'U', '', '', '', '', '', '', '', ''),
(22, 'V', '', '', '', '', '', '', '', ''),
(23, 'X', '', '', '', '', '', '', '', ''),
(24, 'Z', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE IF NOT EXISTS `funcionario` (
  `idFuncionario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_func` varchar(30) NOT NULL,
  `endereco` varchar(30) DEFAULT NULL,
  `bairro` varchar(30) DEFAULT NULL,
  `rg` varchar(30) NOT NULL,
  `cpf` varchar(30) NOT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `senha` varchar(30) NOT NULL,
  `salario` double DEFAULT NULL,
  `tipo` int(10) unsigned NOT NULL,
  `excluir` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idFuncionario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`idFuncionario`, `nome_func`, `endereco`, `bairro`, `rg`, `cpf`, `telefone`, `email`, `senha`, `salario`, `tipo`, `excluir`) VALUES
(1, 'severino', 'aaa', 'ssss', '234234', '2342342', '991106299', 'severino_souza@outlook.com', '123', 1111, 1, 0),
(4, 'Neto', 'Rua Antônio Reto; N 122', 'Iracy', '121232', '3423232', '993750971', 'neto@gmail.com', '123', 1212, 2, 1),
(5, 'Antônio Pedro Adegas Neto', 'Rua Antônio Reto; N 122', 'Iracy', '24242420', '02402402420', '91242424', 'neto@gmail.com', '123', 1000, 2, 0),
(6, 'Anjsd', '', '', '', '', '', '', '', 0, 1, 1),
(7, 'Mansas', '', '', '', '27349823749', '', '', '', 0, 1, 0),
(8, 'Kmask', '', '', '', '09876543356', '', '', '', 0, 1, 0),
(9, 'Maasa', '', '', '', '12142344234', '', '', '', 0, 1, 0),
(10, 'Mrsdfsaaa', '', '', '', '22342342342', '', '', '', 0, 1, 0),
(11, 'Lassa', '', '', '', '23423424234', '', '', '', 0, 1, 0),
(12, 'Leweaaa', '', '', '', '12124575656', '', '', '', 0, 1, 0),
(13, 'Mrdfaaqwqw', '', '', '', '12769789789', '', '', '', 0, 1, 0),
(14, 'dldlasd', '', '', '67867867', '23423423423', '', '', '', 0, 1, 0),
(15, 'Lasasa', '', '', '', '75532424242', '', '', '', 0, 1, 0),
(16, 'Pldsd', '', '', '234', '43647578708', '', '', '', 0, 1, 0),
(17, 'Kasassds', '', '', '', '96989785756', '', '', '', 0, 1, 0),
(18, 'Aaasasa', '', '', '', '35786786767', '', '', '', 0, 1, 1),
(19, 'Qweaasda', '', '', '', '34536456464', '', '', '', 0, 1, 0),
(20, 'Waasdaa', '', '', '', '24345456456', '', '', '', 0, 1, 0),
(21, 'Wewasdasda', '', '', '', '25345345345', '', '', '', 0, 1, 0),
(22, 'Rasdasda', '', '', '', '14233456454', '', '', '', 0, 1, 0),
(23, 'Wasas', '', '', '', '13423423423', '', '', '', 0, 1, 0),
(24, 'Easas', '', '', '', '24242423424', '', '', '', 0, 1, 0),
(25, 'aurelio', 'luzia', 'luzia', '25252', '65355', '52542452', 'aureio@gmail.com', '123', 123, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

CREATE TABLE IF NOT EXISTS `pagamento` (
  `venda_idVenda` int(10) unsigned NOT NULL,
  `dinheiro` double NOT NULL,
  `cartao` double NOT NULL,
  `crediario` double NOT NULL,
  KEY `pagamento_FKIndex1` (`venda_idVenda`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pagamento`
--

INSERT INTO `pagamento` (`venda_idVenda`, `dinheiro`, `cartao`, `crediario`) VALUES
(33, 6, 6, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `parcela`
--

CREATE TABLE IF NOT EXISTS `parcela` (
  `idparcela` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `despesas_iddespesas` int(10) unsigned NOT NULL,
  `data_2` date NOT NULL,
  `status_2` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idparcela`),
  KEY `parcela_FKIndex1` (`despesas_iddespesas`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=234 ;

--
-- Extraindo dados da tabela `parcela`
--

INSERT INTO `parcela` (`idparcela`, `despesas_iddespesas`, `data_2`, `status_2`) VALUES
(233, 23, '2017-01-14', 1),
(232, 22, '2017-01-13', 1),
(231, 21, '2017-03-16', 0),
(230, 21, '2017-02-16', 0),
(229, 21, '2017-01-16', 1),
(228, 20, '2017-12-15', 0),
(227, 20, '2017-11-15', 0),
(226, 20, '2017-10-15', 0),
(225, 20, '2017-09-15', 0),
(224, 20, '2017-08-15', 0),
(212, 17, '2017-02-09', 0),
(211, 17, '2017-01-09', 1),
(210, 16, '2017-02-08', 0),
(223, 20, '2017-07-15', 0),
(222, 20, '2017-06-15', 0),
(221, 20, '2017-05-15', 0),
(220, 20, '2017-04-15', 0),
(219, 20, '2017-03-15', 0),
(218, 20, '2017-02-15', 0),
(217, 20, '2017-01-15', 1),
(216, 19, '2017-01-10', 1),
(215, 18, '2017-03-11', 0),
(214, 18, '2017-02-11', 0),
(213, 18, '2017-01-11', 1),
(209, 16, '2017-01-08', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `idProduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fornecedor_idFornecedor` int(10) unsigned DEFAULT NULL,
  `nome_prod` varchar(30) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `quantidade` int(10) unsigned DEFAULT NULL,
  `data_entrada` date DEFAULT NULL,
  `preco` double DEFAULT NULL,
  `imagem` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idProduto`),
  KEY `produto_FKIndex1` (`fornecedor_idFornecedor`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idProduto`, `fornecedor_idFornecedor`, `nome_prod`, `descricao`, `quantidade`, `data_entrada`, `preco`, `imagem`) VALUES
(4, 1, 'Brico ana ajsnasjasna ajsdna d', '', 30, '2016-10-06', 50, '4.'),
(3, 1, 'Anel', 'Anel folheado', 37, '2016-10-06', 12, '3.jpg'),
(5, 0, 'Vibrador', '', 39, '2016-10-16', 15.5, '5.'),
(6, 0, 'Bacia', '', 0, '2016-10-21', 7, '6.'),
(7, 0, 'Cachimbo', '', 0, '2016-10-21', 4.5, '7.'),
(8, 0, 'Dado', '', 0, '2016-10-21', 2, '8.'),
(9, 0, 'Estilete', '', 0, '2016-10-21', 3, '9.'),
(10, 0, 'Faca', '', 0, '2016-10-21', 5, '10.'),
(11, 0, 'Hipopotamo', '', 0, '2016-10-21', 50, '11.'),
(12, 0, 'Garrafa', '', 0, '2016-10-21', 7.8, '12.'),
(13, 0, 'Jacaré', '', 0, '2016-10-21', 7, '13.'),
(14, 0, 'Idiota', '', 0, '2016-10-21', 3.5, '14.'),
(15, 0, 'Macaco', '', 0, '2016-10-21', 60, '15.'),
(16, 0, 'Livro', '', 0, '2016-10-21', 4.5, '16.'),
(17, 0, 'Navio', '', 0, '2016-10-21', 9.8, '17.'),
(18, 0, 'Oculos', '', 0, '2016-10-21', 3.8, '18.'),
(19, 0, 'Patins', '', 0, '2016-10-21', 6.7, '19.'),
(20, 0, 'Radio', '', 0, '2016-10-21', 5.8, '20.'),
(21, 0, 'Quati', '', 0, '2016-10-21', 2.9, '21.'),
(22, 0, 'Sapo', '', 0, '2016-10-21', 6.9, '22.'),
(23, 0, 'Urubu', '', 0, '2016-10-21', 0, '23.'),
(24, 0, 'Tatu', '', 0, '2016-10-21', 5.9, '24.'),
(25, 0, 'Xuxa', '', 0, '2016-10-21', 10.9, '25.'),
(26, 0, 'Zebra', '', 0, '2016-10-21', 45.5, '26.'),
(27, 0, 'Capa', '', 0, '2016-10-21', 10, '27.'),
(28, 0, 'Aa', '', 1, '2017-01-13', 23, '28.jpg'),
(29, 0, 'Aab', '', 1, '2017-01-13', 10, '29.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto_has_venda`
--

CREATE TABLE IF NOT EXISTS `produto_has_venda` (
  `produto_idProduto` int(10) unsigned NOT NULL,
  `venda_idVenda` int(10) unsigned NOT NULL,
  `quantidade` int(10) unsigned NOT NULL,
  PRIMARY KEY (`produto_idProduto`,`venda_idVenda`),
  KEY `produto_has_venda_FKIndex1` (`produto_idProduto`),
  KEY `produto_has_venda_FKIndex2` (`venda_idVenda`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto_has_venda`
--

INSERT INTO `produto_has_venda` (`produto_idProduto`, `venda_idVenda`, `quantidade`) VALUES
(3, 54, 1),
(4, 50, 1),
(3, 50, 1),
(3, 47, 1),
(4, 47, 1),
(5, 47, 1),
(4, 49, 1),
(3, 48, 1),
(3, 43, 1),
(3, 45, 1),
(3, 34, 1),
(4, 37, 1),
(3, 33, 1),
(4, 32, 1),
(3, 32, 1),
(4, 52, 2),
(3, 53, 1),
(4, 53, 1),
(4, 55, 1),
(5, 56, 1),
(3, 57, 1),
(4, 58, 1),
(5, 59, 1),
(3, 60, 1),
(4, 61, 1),
(3, 62, 1),
(4, 63, 1),
(5, 64, 1),
(5, 65, 1),
(3, 66, 1),
(3, 67, 1),
(5, 68, 1),
(4, 68, 1),
(3, 69, 1),
(4, 70, 1),
(3, 70, 1),
(5, 71, 1),
(4, 71, 1),
(3, 72, 1),
(5, 73, 1),
(3, 74, 1),
(4, 74, 1),
(5, 75, 1),
(4, 75, 1),
(3, 76, 1),
(3, 77, 1),
(3, 78, 1),
(4, 79, 1),
(4, 80, 1),
(5, 81, 1),
(5, 82, 1),
(4, 83, 1),
(3, 84, 1),
(3, 85, 1),
(4, 86, 1),
(5, 86, 1),
(3, 87, 1),
(3, 96, 1),
(4, 89, 1),
(3, 97, 1),
(4, 90, 1),
(3, 95, 1),
(4, 100, 1),
(4, 99, 1),
(3, 99, 1),
(3, 101, 1),
(4, 156, 1),
(4, 102, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `prolabore`
--

CREATE TABLE IF NOT EXISTS `prolabore` (
  `idprolabore` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `funcionario_idFuncionario` int(10) unsigned NOT NULL,
  `valor_prol` double NOT NULL,
  `data_prol` date NOT NULL,
  PRIMARY KEY (`idprolabore`),
  KEY `prolabore_FKIndex1` (`funcionario_idFuncionario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `prolabore`
--

INSERT INTO `prolabore` (`idprolabore`, `funcionario_idFuncionario`, `valor_prol`, `data_prol`) VALUES
(7, 5, 3, '2016-10-19'),
(2, 5, 50, '2016-10-13'),
(3, 5, 25, '2016-09-13'),
(4, 5, 30, '2016-09-13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE IF NOT EXISTS `venda` (
  `idVenda` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_idcliente` int(10) unsigned NOT NULL,
  `funcionario_idFuncionario` int(10) unsigned NOT NULL,
  `data_venda` date NOT NULL,
  `hora_venda` time NOT NULL,
  `pagamento` int(10) unsigned NOT NULL,
  `total_venda` double DEFAULT NULL,
  `status_2` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idVenda`),
  KEY `venda_FKIndex1` (`funcionario_idFuncionario`),
  KEY `venda_FKIndex2` (`cliente_idcliente`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=158 ;

--
-- Extraindo dados da tabela `venda`
--

INSERT INTO `venda` (`idVenda`, `cliente_idcliente`, `funcionario_idFuncionario`, `data_venda`, `hora_venda`, `pagamento`, `total_venda`, `status_2`) VALUES
(43, 1, 1, '2016-09-20', '23:56:16', 4, 12, 1),
(45, 0, 1, '2016-10-20', '13:35:47', 1, 12, 1),
(37, 0, 1, '2016-10-20', '23:56:32', 1, 5, 1),
(34, 0, 1, '2016-10-20', '09:24:16', 1, 12, 1),
(47, 0, 5, '2016-10-20', '02:55:34', 0, 0, 0),
(33, 0, 1, '2016-10-20', '08:08:23', 3, 12, 1),
(32, 0, 1, '2016-10-20', '01:27:17', 1, 17, 1),
(48, 0, 1, '2016-10-20', '18:09:20', 1, 12, 1),
(49, 0, 1, '2016-10-20', '18:09:28', 2, 5, 1),
(50, 1, 1, '2016-10-20', '18:09:57', 4, 17, 1),
(52, 1, 1, '2016-10-20', '18:36:15', 4, 10, 1),
(53, 0, 1, '2016-10-20', '00:17:50', 1, 62, 1),
(54, 0, 1, '2016-10-20', '00:17:58', 1, 12, 1),
(55, 0, 1, '2016-10-20', '00:18:03', 1, 50, 1),
(56, 0, 1, '2016-10-20', '00:18:08', 1, 15.5, 1),
(57, 0, 1, '2016-10-20', '00:18:13', 1, 12, 1),
(58, 0, 1, '2016-10-20', '00:18:18', 1, 50, 1),
(59, 0, 1, '2016-10-20', '00:18:25', 1, 15.5, 1),
(60, 0, 1, '2016-10-20', '00:18:33', 2, 12, 1),
(61, 0, 1, '2016-10-20', '00:18:39', 1, 50, 1),
(62, 0, 1, '2016-10-20', '00:18:45', 2, 12, 1),
(63, 0, 1, '2016-10-20', '00:18:53', 1, 50, 1),
(64, 0, 1, '2016-10-21', '00:19:02', 2, 15.5, 1),
(65, 0, 1, '2016-10-21', '00:19:07', 1, 15.5, 1),
(66, 0, 1, '2016-10-21', '00:19:28', 2, 12, 1),
(67, 0, 1, '2016-10-21', '00:19:35', 2, 12, 1),
(68, 0, 1, '2016-10-21', '00:19:43', 2, 65.5, 1),
(69, 0, 1, '2016-10-21', '00:19:57', 1, 12, 1),
(70, 0, 1, '2016-10-21', '00:20:07', 1, 62, 1),
(71, 0, 1, '2016-10-21', '00:20:13', 1, 65.5, 1),
(72, 0, 1, '2016-10-21', '00:20:23', 2, 12, 1),
(73, 0, 1, '2016-10-21', '00:20:31', 1, 15.5, 1),
(74, 0, 1, '2016-10-21', '00:20:39', 1, 62, 1),
(75, 0, 1, '2016-10-21', '00:20:45', 1, 65.5, 1),
(76, 0, 1, '2016-10-21', '02:48:32', 1, 12, 1),
(77, 0, 1, '2016-10-21', '02:48:37', 1, 12, 1),
(78, 0, 1, '2016-10-21', '02:48:44', 1, 12, 1),
(79, 0, 1, '2016-10-21', '02:48:50', 1, 50, 1),
(80, 0, 1, '2016-10-21', '02:48:56', 1, 50, 1),
(81, 0, 1, '2016-10-21', '02:49:01', 1, 15.5, 1),
(82, 0, 1, '2016-10-21', '02:49:06', 1, 15.5, 1),
(83, 0, 1, '2016-10-21', '02:49:13', 1, 50, 1),
(84, 0, 1, '2016-10-21', '02:49:18', 1, 12, 1),
(85, 0, 1, '2016-10-23', '04:57:54', 1, 12, 1),
(86, 1, 1, '2016-10-31', '17:43:06', 4, 65.5, 1),
(87, 0, 1, '2016-10-31', '17:43:38', 1, 12, 1),
(96, 0, 1, '2016-11-05', '17:51:52', 2, 12, 1),
(89, 0, 1, '2016-11-02', '00:08:03', 1, 48, 1),
(90, 1, 1, '2016-11-05', '16:10:15', 4, 30, 1),
(95, 0, 1, '2016-11-05', '17:32:35', 1, 12, 1),
(97, 0, 1, '2016-11-05', '17:52:28', 2, 7, 1),
(98, 1, 1, '2016-11-16', '01:51:35', 4, 12, 1),
(99, 0, 1, '2016-11-16', '01:53:14', 1, 60, 1),
(100, 0, 1, '2016-11-16', '01:54:23', 2, 50, 1),
(101, 1, 1, '2016-11-16', '01:55:55', 4, 12, 1),
(102, 0, 1, '2017-01-14', '17:42:19', 1, 50, 1),
(103, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(104, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(105, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(106, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(107, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(108, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(109, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(110, 0, 0, '2016-12-10', '03:22:02', 0, 0, 0),
(111, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(112, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(113, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(114, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(115, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(116, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(117, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(118, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(119, 0, 0, '2016-12-10', '03:22:03', 0, 0, 0),
(120, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(121, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(122, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(123, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(124, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(125, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(126, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(127, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(128, 0, 0, '2016-12-10', '03:22:04', 0, 0, 0),
(129, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(130, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(131, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(132, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(133, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(134, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(135, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(136, 0, 0, '2016-12-10', '03:22:05', 0, 0, 0),
(137, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(138, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(139, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(140, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(141, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(142, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(143, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(144, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(145, 0, 0, '2016-12-10', '03:22:06', 0, 0, 0),
(146, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(147, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(148, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(149, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(150, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(151, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(152, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(153, 0, 0, '2016-12-10', '03:22:07', 0, 0, 0),
(154, 0, 0, '2016-12-10', '03:22:08', 0, 0, 0),
(155, 0, 0, '2016-12-10', '03:22:08', 0, 0, 0),
(156, 1, 1, '2017-01-14', '18:09:39', 4, 50, 1),
(157, 0, 1, '2017-01-14', '18:09:39', 0, 0, 0);